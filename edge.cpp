#include "edge.h"

Edge::Edge(City from, City to, int maph, int mapw){
    this->left = from;
    this->right = to;
    int distx = abs(this->left.x - this->right.x);
    int disty = abs(this->left.y - this->right.y);
    this->weight = (distx > (mapw / 2)) ? mapw - distx : distx;
    this->weight += (disty > (maph / 2)) ? maph - disty : disty;
}
