#ifndef TOWNMAP_H
#define TOWNMAP_H

#include <list>
#include <algorithm>
#include <iterator>
#include <numeric>

#include "city.h"
#include "edge.h"

class Townmap
{
public:
    int w;
    int h;
    std::list<City> *cities;
    std::list<Edge> *roads;
    Townmap();
    bool buildRoads();
    int doMap();
};

#endif // TOWNMAP_H
