#ifndef EDGE_H
#define EDGE_H

#include "city.h"
#include <stdlib.h>


class Edge
{
public:
    City left;
    City right;
    int weight;
    Edge(City from, City to, int maph, int mapw);
    bool operator==(const Edge &other) const {
        return ((other.left == this->left) && (other.right == this->right)) ||
               ((other.left == this->right && (other.right == this->left)));
    }
};

#endif // EDGE_H
