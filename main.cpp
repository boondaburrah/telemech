#include <stdio.h>
#include <stdlib.h>
#include "townmap.h"


int main(int argc, char** argv){
    int citycount;
    int width;
    int height;
    // int *cityload;
    Townmap *thisLand;

    if(!scanf("%d %d %d", &citycount, &height, &width)){
        fprintf(stderr, "reading town map header failed.\n");
        return -1;
    }

    /*cityload = (int*)malloc(citycount * 2);
    for(int i = 0; i < (citycount * 2) -1; i = i + 2){
        if(scanf("%d %d", (cityload + i), (cityload+i+1)) != EOF){
            fprintf(stderr, "added city at (%d, %d) to array at slots %d and %d.\n",
                    *(cityload + i), *(cityload + i + 1), i, i+1);
        } else {
            fprintf(stderr, "ERROR: Reached end of city list before expected.\n");
            return -1;
        }
    }*/

    fprintf(stderr, "Found %d cities in a %d by %d map.\n", citycount, width, height);

    // DO CODE SHIT HERE
    thisLand = new Townmap();
    thisLand->w = width;
    thisLand->h = height;
    for(int i = 0; i < citycount; i++){
        City *newton = new City();
        scanf("%d %d", &(newton->x), &(newton->y));
        thisLand->cities->push_back(*newton);
    }
    fprintf(stderr, "Loaded cities.\n");

    thisLand->buildRoads();
    fprintf(stderr, "Built  roads.\n");

    int result = thisLand->doMap();

    printf("%d\n", result);

    //free(cityload);

    return 0;
}
