TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    townmap.cpp \
    edge.cpp

HEADERS += \
    townmap.h \
    city.h \
    edge.h
