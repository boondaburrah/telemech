#include "townmap.h"

Townmap::Townmap()
{
    this->cities = new std::list<City>();
    this->roads = new std::list<Edge>();
}

bool Townmap::buildRoads(){
    std::for_each(this->cities->begin(), this->cities->end(), [this](City &from){
        std::for_each(this->cities->begin(), this->cities->end(), [this, from](City &to){
            if(!(from == to)){
                Edge *construction = new Edge(from, to, this->h, this->w);
                this->roads->push_back(*construction);
            }
        });
    });

    this->roads->sort([](const Edge &mine, const Edge &yours){
        return mine.weight < yours.weight;
    });

    //this->roads->unique();

    return true; // fix this when you actually have some error checking.`
}

int Townmap::doMap(){
    std::list<Edge> *accepted = new std::list<Edge>();
    std::list<City> *visited = new std::list<City>();
    std::list<Edge> *considered = new std::list<Edge>();
    City current = this->roads->front().left;
    int result = 0;
    while(!this->cities->empty()){
        considered->clear();
        if(!considered->empty()){
            fprintf(stderr, "failed to clear considered roads list in loop; expect bad results.");
        }
        this->roads->remove_if([current](Edge e){
            return e.right == current;
        });
        std::copy_if(this->roads->begin(), this->roads->end(), std::back_inserter(*considered), [current, visited](Edge e){
            return e.left == current
                    || std::find(visited->begin(), visited->end(), e.left) != visited->end();
        });
        if(considered->empty()){
            break;
        }
        accepted->push_back(considered->front());
        result += considered->front().weight;
        visited->push_back(current);
        this->cities->remove(current);
        current = considered->front().right;
    }

    return result;
}
