#ifndef CITY_H
#define CITY_H

typedef struct City{
    int x;
    int y;
    bool operator==(const City &other) const {
        return (other.x == x) && (other.y == y);
    }
} City;

#endif // CITY_H
